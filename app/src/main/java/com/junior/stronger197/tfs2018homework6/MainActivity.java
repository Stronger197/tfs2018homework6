package com.junior.stronger197.tfs2018homework6;

import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_1).setOnClickListener(onFirstButtonClick);
        findViewById(R.id.button_2).setOnClickListener(onSecondButtonClick);
        findViewById(R.id.button_3).setOnClickListener(onThirdButtonClick);
    }

    View.OnClickListener onFirstButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Looper.prepare();
                    Looper workerLooper = Looper.myLooper();

                    MyObservable.from(new Runnable() {
                        @Override
                        public void run() {
                            for(int i = 0; i < 5; i++) {
                                Log.e("SOME_TEST", "i is: " + i + " | Thread name: " + Thread.currentThread().getName());
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).subscribeOn(workerLooper).observeOn(Looper.getMainLooper()).subscribe(new MyCallback() {
                        @Override
                        public void run() {
                            Log.e("SOME_TEST", "Callback invoke() | Thread name: " + Thread.currentThread().getName());
                        }
                    });
                }
            }).start();
        }
    };

    View.OnClickListener onSecondButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Looper.prepare();
                    Looper workerLooper = Looper.myLooper();

                    MyObservable.from(new Runnable() {
                        @Override
                        public void run() {
                            for(int i = 0; i < 5; i++) {
                                Log.e("SOME_TEST", "i is: " + i + " | Thread name: " + Thread.currentThread().getName());
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).subscribeOn(workerLooper).subscribe(new MyCallback() {
                        @Override
                        public void run() {
                            Log.e("SOME_TEST", "Callback invoke() | Thread name: " + Thread.currentThread().getName());
                        }
                    });
                }
            }).start();
        }
    };

    View.OnClickListener onThirdButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    MyObservable.from(new Runnable() {
                        @Override
                        public void run() {
                            for(int i = 0; i < 5; i++) {
                                Log.e("SOME_TEST", "i is: " + i + " | Thread name: " + Thread.currentThread().getName());
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).subscribe(new MyCallback() {
                        @Override
                        public void run() {
                            Log.e("SOME_TEST", "Callback invoke() | Thread name: " + Thread.currentThread().getName());
                        }
                    });
                }
            }).start();
        }
    };
}
