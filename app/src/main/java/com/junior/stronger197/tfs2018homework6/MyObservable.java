package com.junior.stronger197.tfs2018homework6;

import android.os.Handler;
import android.os.Looper;

public class MyObservable {

    private final Runnable runnable;
    private Looper starterLooper;
    private Looper resultLooper;

    private MyObservable(Runnable runnable) {
        this.runnable = runnable;

    }


    public static MyObservable from(Runnable runnable) {
        return new MyObservable(runnable);
    }

    public MyObservable subscribeOn(Looper looper) {
        this.starterLooper = looper;
        return this;
    }

    public MyObservable observeOn(Looper looper) {
        this.resultLooper = looper;
        return this;
    }

    public void subscribe(final MyCallback myCallback) {
        if(starterLooper == null) {
            Looper.prepare();
            starterLooper = Looper.myLooper();
        }
        Handler handler = new Handler(starterLooper);
        handler.post(new Runnable() {
            @Override
            public void run() {
                runnable.run();
                if(resultLooper == null) {
                    resultLooper = starterLooper;
                }
                Handler callbackHandler = new Handler(resultLooper);
                callbackHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        myCallback.run();
                    }
                });
                Looper.loop();
            }
        });

        Looper.loop();
    }
}
